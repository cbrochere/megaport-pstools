#
# The following is an exmaple of how to take the json output from the Get-MegaportBandWidth command and 
# turn it into visual graph.

# add data to chart
$datafile = "${env:USERPROFILE}\Documents\EXAMPLE_PORT.json" ;
$dp = Get-Content -Path $datafile | ConvertFrom-Json

# load the appropriate assemblies
[void][Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 
[void][Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms.DataVisualization")

 create chart object 
$Chart = New-object System.Windows.Forms.DataVisualization.Charting.Chart 
$Chart.Width = 900 
$Chart.Height = 300 
$Chart.Left = 40 
$Chart.Top = 30

# create a chartarea to draw on and add to chart 
$ChartArea = New-Object System.Windows.Forms.DataVisualization.Charting.ChartArea 
$Chart.ChartAreas.Add($ChartArea)

[void]$Chart.Series.Add("in_mbps")
[void]$Chart.Series.Add("out_mbps") 
#$Chart.Series["Data"].Points.DataBind($dp.in_mbps)
$Chart.Series["in_mbps"].Points.DataBindY($dp.in_mbps)
$Chart.Series["in_mbps"].ChartType = [System.Windows.Forms.DataVisualization.Charting.SeriesChartType]::SplineArea  ;
$Chart.Series["out_mbps"].Points.DataBindY($dp.out_mbps)
$Chart.Series["out_mbps"].ChartType = [System.Windows.Forms.DataVisualization.Charting.SeriesChartType]::SplineArea ;

# display the chart on a form 
$Chart.Anchor = [System.Windows.Forms.AnchorStyles]::Bottom -bor [System.Windows.Forms.AnchorStyles]::Right -bor 
                [System.Windows.Forms.AnchorStyles]::Top -bor [System.Windows.Forms.AnchorStyles]::Left 
$Form = New-Object Windows.Forms.Form 
$Form.Text = "PowerShell Chart"
$Form.Width = 1000 
$Form.Height = 400 
$Form.controls.add($Chart) 
$Form.Add_Shown({$Form.Activate()}) 
$Form.ShowDialog();
