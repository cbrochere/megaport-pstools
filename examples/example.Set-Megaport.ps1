﻿Clear-Host ; #cls

# Import the module using -Force is Required while editing the deployed version in your current PSSession
Import-Module "Megaport" -Force ;

# Login Credentials
$login = Login-MegaportAccount `
    -Username 'chris@brochere.co.nz' ` # email address
    -ApiServer "api-staging.megaport.com" ` # [optional], useful for testing on staging
    -Password $(Get-Content -LiteralPath ${env:USERPROFILE}\credentials\staging.megaport.txt) # I prefer to store my password in an encrypted folder

# For testing, the following has been created and are active
$portA   = "EXAMPLE_PORT_NZ1" ;
$portB   = "EXAMPLE_PORT_NZ2" ;
$vxcName = "EXAMPLE VXC NZ1-NZ2" ;

# Filter the product list by-name using $portA name
$p = Get-Megaport -AuthObject $login | Where-Object { $_.productName -eq $portA }

# Here we want a specific Vxc
$v = $p.associatedVxcs | Where-Object { $_.productName -eq $VxcName }

# here we are only changinig the ratelimit for a given Vxc
$Body = @{
    productName=$v.productName
    productUid=$v.productUid
    rateLimit=1
}

# check if the Vxc ratelimit 
If ( $Body.rateLimit -ne $v.rateLimit ) {
    Set-Megaport -AuthObject $login -InputObject $Body -ProductType Vxc
} Else {
    Write-Warning -Message "New and Old ratelimits are the same. Nothing to update"
}
