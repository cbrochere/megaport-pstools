Clear-Host ; #cls

# Remove the module from memory, then import it again. Required while editing the deployed
# version in your current PSSession
Remove-Module "Megaport" -Verbose -Force ;
Import-Module "Megaport" -Verbose -Force ;

$login = Login-MegaportAccount `
    -Username 'chris@brochere.co.nz' `
    -ApiServer "api-staging.megaport.com" `
    -Password $(Get-Content -LiteralPath ${env:USERPROFILE}\credentials\staging.megaport.txt)

# Get a list of all existing products
Get-Megaport -AuthObject $login

# Filter products to a single product, by-name.
$port = Get-Megaport -AuthObject $login | Where-Object { $_.Name -eq "Example Spark/Revera Port1" }

Get-Megaport -AuthObject $login -productUid $port.productUid