Clear-Host ; #cls

# Import the module using -Force is Required while editing the deployed
# version in your current PSSession
Import-Module "Megaport" -Verbose -Force ;

$login = Login-MegaportAccount `
    -Username 'chris@brochere.co.nz' `
    -ApiServer "api-staging.megaport.com" `
    -Password $(Get-Content -LiteralPath ${env:USERPROFILE}\credentials\staging.megaport.txt)

# Add-Megaport works directly off an object that wil be converted to JSON   
$order = @(
    @{
      "productName"="Test Port"
      "term"=1
      "productType"="MEGAPORT"
      "portSpeed"=1000
      "locationId"=81
    } 
);

# validate the order
Add-Megaport -AuthObject $login -Order $order -Test -Verbose

# Place the order
#Add-Megaport -AuthObject $login -Order $order -Verbose