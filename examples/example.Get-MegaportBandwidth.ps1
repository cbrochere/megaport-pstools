Clear-Host ; #cls

# Remove the module from memory, then import it again. Required while editing the deployed
# version in your current PSSession
Import-Module "Megaport" -Verbose -Force ;

$login = Login-MegaportAccount `
    -Username 'chris@brochere.co.nz' `
    -ApiServer "api-staging.megaport.com" `
    -Password $(Get-Content -LiteralPath ${env:USERPROFILE}\credentials\staging.megaport.txt)


# filter to a  particular product
$productName = ''
$Product = Get-Megaport -AuthObject $login | Where-Object { $_.Name -eq $productName} 

$msec = @{}
$msec.endEpoch = ([Math]::Round((Get-Date).ToFileTime()/10000))
$msec.startEpoch   = ([Math]::Round(((Get-Date).AddDays(-30)).ToFileTime()/10000))

Get-MegaportBandwidth -AuthObject $login -InputObject $Product -endEpoch $msec.endEpoch -startEpoch $msec.startEpoch 