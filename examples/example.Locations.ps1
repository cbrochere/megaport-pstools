Clear-Host ; #cls

# Remove the module from memory, then import it again. Required while editing the deployed
# version in your current PSSession
Remove-Module "Megaport" -Verbose -Force ;
Import-Module "Megaport" -Verbose -Force ;

$login = Login-MegaportAccount `
    -Username 'chris@brochere.co.nz' `
    -ApiServer "api-staging.megaport.com" `
    -Password $(Get-Content -LiteralPath ${env:USERPROFILE}\credentials\staging.megaport.txt)

# Get a list of all existing products
Get-Megaport -AuthObject $login

# Get a list of a specific product by Uid
Get-Megaport -AuthObject $login -productUid '21078fcc-a23f-4879-af02-2fcd7dc4c8db'

# Get a list of all locations, then filter by metropolitan area name
Get-MegaportLocation -AuthObject $login | Where-Object { $_.Metro -eq 'Wellington' }

# Retutn the Id number of a specific Datacentre by name
(Get-MegaportLocation -AuthObject $login | Where-Object { $_.Name -eq 'Spark/Revera Takanini' }).id