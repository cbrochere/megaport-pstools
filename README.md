# megaport-pstools #

Megaport PowerShell Tools for automation and scripting.

This started life with the purpose of figuring out how one might schedule a bandwidth change on a VXC, but then blew-up into various other tools to simplify other requests from users and general tasks such as exporting and graphing the bandwidth usage and a quick-and-dirty way to validate state of various things.

While the Megaport UX at [https://megaport.al] is really great, simple and intuitive - there's always a need for scripted automation with integration with other powershell suites such as the Azure PowerShell Tools


## Commands ##
*Login-MegaportAccount* _or_ *Get-MegaportAccount*

*Add-Megaport* - Adds services such as Ports, VXCs or IXs as per the defined JSON object

*Get-Megaport* - Get an object containing all the products in the users account

*Get-MegaportLocation* - Obtain a list of Megaport locations

*Get-MegaportInterfaceLogs* - Obtains the log information for a specified productUid

*Get-MegaportBandwidth* - Obtains the bandwidth stats into a Json object. parse with | ConvertTo-Json -Depth 10 | Out-File $filename

*Set-Megaport* - Apply changes to existing services

## Installation ##

1.  Copy the module files to ${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\
2.  In PowerShell Script, use Import-Module "Megaport"

## Examples ##
There are examples in the example folder

Get-Help Cmd-Let -examples 

# Disclaimer #
The author does not work for Megaport, nor are these tools approved by, sanctioned or commissioned for use with the Megaport API. The use of the name megaport-pstools is not an endorsement from Megaport, it serves to only identify clearly the purpose of the tools. A 'Megaport' is defined as a noun identifing a physical port provided by Megaport(the entity).

# Permissions
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
2. Credit for the work by contributers and authors is recognized
3. Neither the name of any Author, Contributor, or 'Megaport' may be used to endorse or promote products derived from this software without specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.