﻿Function Get-Megaport {

    <#
    .SYNOPSIS
    Get information on a specific Megaport 'port' or 'vxc'

    .DESCRIPTION
    This cmdlet enables users to display information on existing ports, VXCs or IXs.

    .PARAMETER AuthObject
    The hashtable containing the authentication information. Use $login = Login-MegaportAccount to create this object

    .PARAMETER productUid
    Display information on the service with sepcified productUid.
   
    .LINK
    https://dev.megaport.com
    https://www.megaport.com

    .EXAMPLE
    Obtain a list of all products and services, including decommissioned.
    Get-Megaport -AuthObject $login
    
    .EXAMPLE
    Get information on a specific product by Name
    $product = Get-Megaport -AuthObject $login | Where-Object { $_.productName -eq "My Megaport" }

    .EXAMPLE
    Get information on a specified product by Uid
    $product = Get-Megaport -AuthObject $login -productUid 12345678-1234-5678-123456789012
    
    .EXAMPLE
    Get details of a Vxc attached to a specific port
    $port = Get-Megaport -AuthObject $login | Where-Object { $_.productName -eq "My Megaport" }
    $vxc = $port.associatedVxcs | Where-Object { $_.productName -eq "My Vxc" }
    Get-Megaport -AuthObject $login -productUid $vxc.productUid
    #>

    [CmdletBinding()]
    
    param
    (
        [Parameter(Mandatory=$True,Position=0)]
        [hashtable]$AuthObject,

        [Parameter()]
        [string]$productUid=$false
    )
    
    Switch($productUid) 
    {
        $false  { $Url = "https://$($AuthObject.ApiServer)/v2/products" ; }
        default { $Url = "https://$($AuthObject.ApiServer)/v2/product/$productUid" ;}
    }
    
    # get an object of the products
    $Response = Invoke-RestMethod -Uri $Url -Method Get -Headers $AuthObject.Headers `
        -WebSession $AuthObject.WebSession `
        -ErrorAction Stop;

    Return $Response.data ;
}