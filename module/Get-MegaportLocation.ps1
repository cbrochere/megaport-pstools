Function Get-MegaportLocation {
    <#
    .SYNOPSIS
    Get-MegaportLocation is used to determine the location of datacentres, partners or IX locations.

    .DESCRIPTION
    Get-MegaportLocation is used to determine the location of datacentres, partners or IX locations.
    
    .PARAMETER AuthObject
    The hashtable containing the Auth details.

    .PARAMETER TimeoutSec
    Number of seconds to timout a session.

    .PARAMETER Type
    The type of Megaport location to lookup. Type is 'Partner' or 'IX'.
    When -Type is 'IX', This will return a list of all available IX Types for a given Location. The parameter -LocationId is then required.
    When -Type is 'Partner', this will return a list of all public partner Megaports. The locationId field will be used in subsequent orders.

    .PARAMETER locationId
    Required paramter when the -Type is 'IX', used to obtain a list of IX types for a given locationId.

    .EXAMPLE
    Obtain a list of all locations Megaport has a PoP
    Get-MegaportLocation -AuthObject $auth

    .EXAMPLE
    Obtain a list of Megaport partners
    Get-MegaportLocation -AuthObject $auth -Type Partners

    .EXAMPLE
    Obtain a list of IX locations, from a partner locationId
    Get-MegaportLocation -AuthObject $auth -Type IX -locationId 300
    
    .LINK
    https://www.megaport.com
    https:/dev.megaport.com

    #>
    [CmdletBinding()]
    
    param
    (
        [Parameter(Mandatory=$True,Position=0)]
        [hashtable]$AuthObject,

        [Parameter()]
        [int]$TimeoutSec=20,

        [Parameter()]
        [ValidateSet("IX","Partner")]
        [string]$Type,

        [Parameter(ParameterSetName="IX")]
        [ValidateNotNullOrEmpty()]
        [string]$locationId=$False
    )

    If ( $Type -ieq "IX" ) {
        If ( $locationId -eq $False ) {
            Write-Error -message "The paramter -LocationId is required when parameter '-Type' is set to 'IX'" ;
            Break ;
        }
    }
    
    Switch($Type) {
        "Ix"      { $Url = "https://$($AuthObject.ApiServer)/v2/product/ix/types?locationId=${locationId}" ; }
        "Partner" { $Url = "https://$($AuthObject.ApiServer)/v2/dropdowns/partner/megaports"; }
        Default   { $Url = "https://$($AuthObject.ApiServer)/v2/locations" ; }
    }

    # get an object of the products
    $Response = Invoke-RestMethod -Uri $Url -Method Get -Headers $AuthObject.Headers -WebSession $AuthObject.WebSession -TimeoutSec $TimeoutSec ; 
       
    Write-Verbose -Message $Response.message ;
    
    Return $Response.data ;
}