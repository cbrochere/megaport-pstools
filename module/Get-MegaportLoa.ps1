﻿Function Get-MegaportLoa {
    <#
    .SYNOPSIS
    Downloads the sepcified Megaport interface LoA (Letter of Authorization) pdf

    .DESCRIPTION
    Downloads the sepcified Megaport interface LoA (Letter of Authorization) pdf

    .PARAMETER AuthObject
    Hashtable containing the authentication information.
   
    .PARAMETER ProductUid
    The productUid of the port/interface you want the logs of.

    .LINK
    https://dev.megaport.com
    https://www.megaport.com

    .EXAMPLE
    $log = Get-MegaportLoa -producUid $port.productUid 
    #>

    [CmdletBinding()]

    param
    (
        [Parameter(Mandatory=$True,Position=0)]
        [hashtable]$AuthObject,
        
        [Parameter(Mandatory=$True)]
        [string]$productUid
    )

    $Uri = "https://$($AuthObject.apiServer)/v2/product/${productId}/loa"

    $Response = Invoke-WebRequest -Headers $AuthObject.Headers -Method Get -Uri $Uri -ContentType "application/json" -WebSession $AuthObject.WebSession ;
    
    Write-Verbose -Message $Response.message ;

    return $Response.data
}