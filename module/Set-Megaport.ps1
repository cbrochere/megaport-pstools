Function Set-Megaport {

    <#
    .SYNOPSIS
    Use this to set or update a Megaport PORT or VXC associated with a Megaport by feeding it the necessary object
    #>

    [CmdletBinding()]

    param
    (
         [Parameter(Mandatory=$True,Position=0)][hashtable]$AuthObject
        ,[Parameter(Mandatory=$True)][hashtable]$InputObject
        ,[Parameter(Mandatory=$True)][ValidateSet("Vxc","Port")][string]$ProductType
    )

    Switch ( $ProductType ) {
        "Vxc"  { $Uri = "https://$($AuthObject.apiServer)/v2/product/vxc/$($InputObject.ProductUid)" }
        "Port" { $Uri = "https://$($AuthObject.apiServer)/v2/product/$($InputObject.ProductUid)" }
    }

    $Body = $InputObject | ConvertTo-Json -Depth 10 ;

    $Response = Invoke-RestMethod -Headers $AuthObject.Headers -Body $Body -Method Put -Uri $Uri `
        -ContentType "application/json" -WebSession $AuthObject.WebSession ;
    
    Write-Verbose -Message $response.message ;

    Return $Response ;
}

Export-ModuleMember -Function * -Alias *