﻿
Function Get-MegaportAccount {

    <#
    .SYNOPSIS
    Creates an authentication hashtable to pass through to all the other Megaport Cmdlets.

    .DESCRIPTION
    Creates an authentication hashtable to pass-through to all the Megaport PS Cmdlets. Takes username and password and logs into api.megaport.com returning a hashtable of the resulting JSON response and header information.

    There are two vanity aliases for this Cmdlet: Login-MegaportAccount and Set-MegaportAccount. Login-MegaportAccount is used often and is borrowed from Login-AzureRmAccount, however "Login" is not an approved Noun.

    .PARAMETER ApiServer
    Use to override the default api.megaport.com. Typically api-staging.megaport.com

    .PARAMETER Username
    Username, usually an email address.

    .PARAMETER Password
    The password for the username. To obscure the password. place it in a an encrypted file or folder and import it with Get-Contents

    .PARAMETER OneTime
    If you have 2FA enabled on the account you are loggin in with, give the token here.
   
    .LINK
    https://dev.megaport.com
    https://www.megaport.com

    .EXAMPLE
    $login = Login-MegaportAccount -Username user@example.com -Password "123456^qwerty" -ApiServer "api-staging.megaport.com"
    #>

    [CmdletBinding()]

    Param 
    (   # if the default needsd to be overridden, define it using the following parameter
        [parameter(Mandatory=$False)]
        [string]$ApiServer="api-staging.megaport.com",

        [parameter(Mandatory=$True)]
        [string]$Username,
        
        [parameter(Mandatory=$True)]
        [string]$Password,

        [parameter(Mandatory=$False)]
        [string]$OneTime
    )

    If ($OneTime) {
        Write-Verbose -Message "Using onetime token" ;
        $Url = "https://${ApiServer}/v2/login?username=${Username}&password=${Password}&oneTimePassword=$OneTime" ;
    } Else {
        $Url = "https://${ApiServer}/v2/login?username=${Username}&password=${Password}" ;
    }

    Try 
    {
        $Response = Invoke-RestMethod -Uri $Url -Method Post `
            -WebSession $WebSession -ErrorAction Stop ;
        Write-Verbose -Message $Response.message ;
    }
    Catch
    {
       $_.Response ;
    }

    $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $headers.Add("X-Auth-Token", ${Response}.Data.token)
    $headers.Add("Content-Type", "application/json")

    Return @{
        WebSession=$WebSession
        Headers=$headers
        Response=$Response
        ApiServer=$ApiServer
    }
}

Set-Alias -Name Login-MegaportAccount -Value Get-MegaportAccount
Set-Alias -Name Set-MegaportAccount -Value Get-MegaportAccount