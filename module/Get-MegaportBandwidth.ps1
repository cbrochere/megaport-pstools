Function Get-MegaportBandwidth {

    <#
    .SYNOPSIS
    Cmdlet for retrieving the Megaport bandwidth stats.

    .DESCRIPTION
    This cmdlet allows users to pull down bandwidth stats for their Megaport services. This returns a JSON object, which is converted to a PSObject which the the user can pass through ConvertTo-Json and Out-File to save to a file; or you can just process the object directly.

    .PARAMETER AuthObject
    The hashtable containing the authentication information. Use Login-MegaportAccount to create this object.

    .PARAMETER InputObject
    The object containing the product details. Typically created from $product = Get-Megaport ...

    .PARAMETER StartEpoch
    The epoch start timestamp in milliseconds for the data range to return
    
    .PARAMETER EndDate 
    The epoch end timestamp in milliseconds for the data range to return

    .LINK
    https://dev.megaport.com
    https://www.megaport.com

    .EXAMPLE
    $Product = Get-Megaport -AuthObject $login | Where-Object { $_.Name -eq $productName} 

    $msec = @{}
    $msec.endEpoch = ([Math]::Round((Get-Date).ToFileTime()/10000))
    $msec.startEpoch   = ([Math]::Round(((Get-Date).AddDays(-30)).ToFileTime()/10000))

    Get-MegaportBandwidth -AuthObject $login -InputObject $Product -endEpoch $msec.endEpoch -startEpoch $msec.startEpoch  
 
    #>

    [CmdletBinding()]
    
    param
    (
        [Parameter(Mandatory=$True,Position=0)][hashtable]$AuthObject,
        [Parameter(Mandatory=$True)][object]$InputObject,
        [Parameter(Mandatory=$True)][string]$endEpoch,
        [Parameter(Mandatory=$True)][string]$startEpoch
    )

    $Uri = "https://$($AuthObject.apiServer)/v2/graph/mbps?productIdOrUid=$($InputObject.ProductUid)&end=$endEpoch&start=$startEpoch"

    $Response = Invoke-RestMethod -Headers $AuthObject.Headers -Method Get -Uri $Uri -ContentType "application/json" -WebSession $AuthObject.WebSession ;
    
    Write-Verbose -Message $Response.message ;

    return $Response.data
}