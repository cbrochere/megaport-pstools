# Import the files as Source

. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Add-Megaport.ps1"
. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Get-MegaportBandwidth.ps1"
. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Get-MegaportAccount.ps1"
. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Get-MegaportLocation.ps1"
. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Get-MegaportInterfaceLogs.ps1"
. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Get-MegaportLoa.ps1"
. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Get-Megaport.ps1"
. "${env:USERPROFILE}\Documents\WindowsPowerShell\Modules\Megaport\Set-Megaport.ps1"

Export-ModuleMember -Cmdlet * -Alias * -Function *