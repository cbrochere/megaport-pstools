Function Add-Megaport {
    <#
    .SYNOPSIS
    Cmdlet for Adding Megaport services.

    .DESCRIPTION
    This cmdlet allows users to validate or provision new or existing ports, VXCs or IXs; or Any service that can be submitted to the rest api using the JSON structure defined at https://dev.megaport.com

    .PARAMETER AuthObject
    The hashtable containing the authentication information. Use Login-MegaportAccount to create this object

    .PARAMETER Order
    Enter an object containing the information of the service you are ordering. This will be converted to JSON, so needs to reflect the JSON structure defined at https://dev.megaport.com
    You may also use the New-MegaportVXC, New-MegaportIX, New-Megaport cmdlets to create minimum required objects.

    .PARAMETER Test
    Runs against the remote validation endpoint and does not place the order. Omitting this places the order (Default).
    
    .LINK
    https://dev.megaport.com
    https://www.megaport.com

    .EXAMPLE
    Add-Megaport -AuthObject $login -Order $order -Test
    Tests if the information provided in $order is valid.

    .EXAMPLE
    Add-Megaport -AuthObject $login -Order $order
    Places an order using the information provided in $order
    #>

    [CmdletBinding()]
    Param(
        [Parameter(
            Mandatory=$True,
            Position=0)]
        [hashtable]$AuthObject,

        [Parameter(
            Mandatory=$True,
            ValueFromPipeline=$True)]
        [object]$Order,

        [Parameter()]
        [switch]$Test=$False
    )

    $Body = $Order | ConvertTo-Json -Depth 100 -Compress ;

    Switch($Test) {
        $True   { $Uri = "https://$($AuthObject.apiServer)/v2/networkdesign/validate" ; }
        default { $Uri = "https://$($AuthObject.apiServer)/v2/networkdesign/buy" ; }
    }  

    $Response = Invoke-RestMethod -Headers $AuthObject.Headers -Method Post `
        -Uri $Uri -WebSession $AuthObject.WebSession `
        -Body $Body -ContentType "application/json"
    
        #Write-Verbose -Message $Response.message ;
    
    Return $Response ;
}